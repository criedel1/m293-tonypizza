import data from '../json/softdrinks.json' assert { type: 'json' }

let cartButtonClickCount = 0;

document.getElementById("choice").addEventListener('click', function (event) {
    if (event.target.classList.contains('cartButton')) {
        cartButtonClickCount++;
        document.getElementById('cartCount').textContent = cartButtonClickCount;
    }
});

document.getElementById("warenkorb").addEventListener('click', function () {
    if (cartButtonClickCount > 0) {
        alert("Vielen Dank für Ihre Bestellung");
        cartButtonClickCount = 0;
        document.getElementById('cartCount').textContent = cartButtonClickCount;
    }
    else{
        alert("Warenkorb leer")
    }
});

for (var i = 0; i < data.length; i++) {
    const cartButton = document.createElement('button');
    cartButton.textContent = 'Add to Cart';
    cartButton.classList.add('cartButton');
}

for (var i = 0; i < data.length; i++){
    document.getElementById("choice").innerHTML += '<div class="sizes"><img class="saladimg" src=' + data[i].imageUrl + '><h3>' + data[i].name +'</h3><div class="opportunity"><div class="pos1"><select><option>' + data[i].volume +'</option></select></div><div class="pos2"><p>' + data[i].prize + '</p><button class="cartButton" id="cartButton"></button></div></div></div>'
}